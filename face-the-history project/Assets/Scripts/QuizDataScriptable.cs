﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This code allows us to create elements in the inspector
// for use inside the game
[CreateAssetMenu(fileName = "QuestionData", menuName = "QuestionData")]
public class QuizDataScriptable : ScriptableObject
{
    public List<Question> questions;
}
