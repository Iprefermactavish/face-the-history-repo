﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PauseMenu : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    // Using a static variable so as not to reference 
    // this variable in the game but using it to check
    // if the game is currently paused
    public static bool GameIsPaused = false;

    // This variable is to control the UI
    public GameObject pauseMenuUI;

    // This variable will let us enter in the inspector
    // what level is to be loaded
    public string levelToLoad;


    //public void PauseButton()
    //{
        // Checking if the game is paused
        // using the escape key
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
            // Game is already paused and Escape pressed
            //if (GameIsPaused)
            //{
                // Resume the game
                //Resume();
            //}
            // Game is not paused when Escape pressed
            //else
            //{
                // Pause the game
                //Pause();
            //}
        //}
    //}

    // Function to Resume the game by
    // pressing the resume button
    // Un-freeze time in the game, close pause menu
    // change GameIsPaused to false
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    // Function to Pause the game by
    // Freeze time in the game, bring up pause menu
    // change GameIsPaused to true
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    // Function to return to the main menu
    // by pressing the menu button
    public void LoadMenu ()
    {
        Debug.Log("Loading Menu...");
        SceneManager.LoadScene(levelToLoad);
    }

    // Function to quit the game
    // by pressing the quit button
    public void QuitGame()
    {
        Debug.Log("Quitting Game");
        Application.Quit();
    }
}
