﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    // This variable will let us enter in the inspector
    // what level is to be loaded
    public string levelToLoad;

    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------

    // Activating a trigger when hit with a collider
    private void OnTriggerEnter(Collider otherCollider)
    {
        //Debug.Log("Object hit the door!");
        // This function is used to tell if an object
        // tagged Player has collided with the object
        if (otherCollider.tag == "Player")
        {
            // if it is a Player: a message in the console
            // and load the level listed in the public variable
            Debug.Log("Player hit the door!");
            SceneManager.LoadScene(levelToLoad);
        }

    }

}
