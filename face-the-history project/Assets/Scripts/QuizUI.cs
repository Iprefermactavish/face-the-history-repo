﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUI : MonoBehaviour
{
    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------

    [SerializeField] private GameManager quizManager;
    [SerializeField] private Text questionText, scoreText;
    [SerializeField] private List<Button> options;
    [SerializeField] private Color correctCOL, wrongCOL, normalCOL;

    private Question question;
    private bool answered;

    // Public variables are visible and editable within the inspector
    // Getting the gameobjects text components
    public Text ScoreText { get { return scoreText; } }

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < options.Count; i++)
        {
            Button localBtn = options[i];
            localBtn.onClick.AddListener(() => OnClick(localBtn));
        }
    }

    public void SetQuestion(Question question)
    {
        this.question = question;
        questionText.text = question.questionInfo;
        List<string> answerList = ShuffleList.ShuffleListItems<string>(question.options);

        for (int i = 0; i < options.Count; i++)
        {
            options[i].GetComponentInChildren<Text>().text = answerList[i];
            options[i].name = answerList[i];
            options[i].image.color = normalCOL;
        }

        answered = false;
    }

    private void OnClick(Button btn)
    {
        if (!answered)
        {
            answered = true;
            bool val = quizManager.Answer(btn.name);

            if (val)
            {
                btn.image.color = correctCOL;
            }
            else
            {
                btn.image.color = wrongCOL;
            }
        }
    }
    


}
