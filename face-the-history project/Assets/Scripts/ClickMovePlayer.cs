﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickMovePlayer : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    // This variable will let us select specific layers
    // we can click on to move our player
    public LayerMask whatCanBeClickedOn;

    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------

    // This is an inbuilt Nav Mesh variable from
    // the Unity Engine AI
    private NavMeshAgent myAgent;


    // ------------------------------------------------
    // Start is called when the script is loaded
    // ------------------------------------------------    
    void Start()
    {
        // Setting the myAgent variable to the NavMeshAgent
        // that is attached to our player character
        myAgent = GetComponent<NavMeshAgent>();
    }

    // ------------------------------------------------
    // Update is called once per frame
    // ------------------------------------------------
    void Update()
    {
        // Checking if we have pressed down the right mouse button
        if (Input.GetMouseButtonDown(0))
        {
            // Casting a ray from the camera to wherever our mouse position is
            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            // This Raycast variable will contain all the information 
            // about what the ray hits
            RaycastHit hitInfo;

            // To actually cast the ray from the camera to the mouse
            if (Physics.Raycast(myRay, out hitInfo, 100, whatCanBeClickedOn))
            {
                myAgent.SetDestination(hitInfo.point);
            }
        }
    }
}
