﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;

public class GameManager : MonoBehaviour
{
    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------

    [SerializeField] private QuizUI quizUI;
    [SerializeField] QuizDataScriptable quizData;

    private List<Question> questions;
    // this variable is for holding the select question
    private Question selectedQuestion;
    // This variable is for setting the index to zero
    private int questionIndex = 0;
    // This variable is for holding the score
    private int scoreCount = 0;
    // This variable is for holding the max score
    private int maxScore = 0;

    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    public string mapScene;
    public string winScene;
    public PlayerHealth player;

    // Start is called before the first frame update
    void Start()
    {
        scoreCount = PlayerPrefs.GetInt("CurrentScore");
        maxScore = PlayerPrefs.GetInt("MaxScore");
        questions = quizData.questions;
        SelectQuestion();
    }

    // This will select a question from the ones we have
    // created within the inspector
    void SelectQuestion()
    {
        // select a random question
        selectedQuestion = questions[questionIndex];
        quizUI.SetQuestion(selectedQuestion);
    }

    // Checking the Answer for the question
    public bool Answer(string answered)
    {
        bool correctAns = false;
        if (answered == selectedQuestion.correctAns)
        {
            // If the answer is correct change the bool to true
            // And add to the score, displaying it on the screen
            correctAns = true;
            scoreCount += 10;
            quizUI.ScoreText.text = "Score: " + scoreCount;
            PlayerPrefs.SetInt("CurrentScore", scoreCount);
        }
        else
        {
            // No 
            // leave the bool as false
            // and player lives go down by 1
            player.ChangeHealth(-1);
        }

        // Adding 1 onto the question index
        questionIndex = questionIndex + 1;
        if (questionIndex < questions.Count)
        {
            // if the question index is less than the number of questions
            // load the next question
            Invoke("SelectQuestion", 0.4f);
        }
        else
        {
            // the question index is more than the number of questions
            // taken to another scene
            SceneManager.LoadScene(mapScene);
        }

        // if the current score ever succeeds or is equal to the max score
        // take the player to another different scene
        if (scoreCount >= maxScore)
        {
            SceneManager.LoadScene(winScene);
        }
        else
        {
            // if not greater than or equal too, load next question
            Invoke("SelectQuestion", 0.4f);
        }

        return correctAns;
    }
}


[System.Serializable]
public class Question
{
    // This class will contain the details of our questions
    public string questionInfo;
    public List<string> options;
    public string correctAns;
}