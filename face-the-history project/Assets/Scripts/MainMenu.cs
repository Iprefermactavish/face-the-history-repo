﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    public int scoreMaximum = 0;

    // This will load another level that is 
    // the next scene listed in the build menu
    public void PlayGame ()
    {
        // Setting the timescale of the game
        Time.timeScale = 1f;
        // setting the player's current score to 0
        PlayerPrefs.SetInt("CurrentScore", 0);
        // setting the max score the player can get
        // to a number set on the inspector
        PlayerPrefs.SetInt("MaxScore", scoreMaximum);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // This function is used to quit the game
    // With a debug to show that it is working in the console
    public void QuitGame ()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
}
