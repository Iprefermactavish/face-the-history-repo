﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------

    // an inbuilt unity engine audio variable
    public AudioMixer audioMixer;

    // This  will take a value from the slider
    // and turn it into the game's volume
    public void SetVolume (float volume)
    {
        //setting the parameter from the audio mixer to the slider value
        audioMixer.SetFloat("volume", volume);
    }

    // Taking the index of the element chosen
    public void SetQuality (int qualityIndex)
    {
        // Accessing the quality settings and replacing it
        // with the index
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    // Making the game fullscreen
    public void SetFullscreen (bool isFullscreen)
    {
        Debug.Log("Going FULLSCREEN!");
        Screen.fullScreen = isFullscreen;
    }
}
