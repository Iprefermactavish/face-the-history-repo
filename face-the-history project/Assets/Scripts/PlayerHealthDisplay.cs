﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthDisplay : MonoBehaviour
{
    // UI Variable that contains a text element
    // that allows us to edit text in unity
    Text healthValue;
    // Variable that allows us to reference the
    // player's health
    PlayerHealth player;
    
    // Start is called before the first frame update
    void Start()
    {
        // Get the text component attached to this object
        // and store it in the variable
        healthValue = GetComponent<Text>();

        // Search the entire scene for the player health
        // component and store it in the variable
        player = FindObjectOfType<PlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        // Get the current health from the player using GetHealth function
        // Changing that number to text using ToString
        // On the health value text component, set text to number retrieved
        healthValue.text = player.GetHealth().ToString();
    }
}
